## Download

geneRFinder has a large file in the repository, git clone may not work on all operating systems, thus, it is advisable to use one of the alternatives below:

- Make the git clone of the repository, download the *model.RData* file [here](https://osf.io/nrs8g/download) and replace in the *src* folder. 

- Install the [Git LFS](https://docs.gitlab.com/ee/topics/git/lfs/). It is necessary to download large file correctly by git clone. 

- Download by our OSF repository [here](https://osf.io/w2yd6/).

In the both cases, make sure that the *src/model.RData* file must be about 128MB in size.


## Installation

To install geneRFinder, please running the script follow:
``` R
Rscript ./src/config.R
```

## Usage

To run geneRFinder, 
``` R
Rscript ./geneRFinder.R -i [fasta_file_name] -o [output_file_name] -t [thread_number] -s [start_type] -n [intergenic]
``` 

<br />
[fasta_file_name]: input file name

<br />
[output_file_name]: output file name

<br />
[thread_number]: number of thread

<br />
[start_type]: <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 - if start codon is ATG <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 - if start codon is ATG, GTG and TTG <br />

<br />
[intergenic]: <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 - output without intergenic sequences <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 - output with intergenic sequences <br />
<br />

For example,

``` R
Rscript ./geneRFinder.R -i ./example/final.contigs.fa -o output -t 7 -s 1 -n 1
``` 


